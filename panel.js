require('rootpath')();

var http = require('http'),
	path = require('path'),	
	express = require('express'),
	session = require('express-session'),
	async = require('async'),
	moment = require('moment'),
	_ = require('underscore'),
	crypto = require('crypto'),
	passport = require('passport'),
	Parse = require('parse').Parse,
	PassportLocalStrategy = require('passport-local').Strategy,
	uuid = require('node-uuid'),
	request = require('request'),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	Q = require('q'),
	clientIp = require('client-ip'),
	mongoSkin = require('mongoskin');

var config = require(path.join('lib','modules', 'Config')).getConfig();
	config.set('rootPath', __dirname);
	config.set('rootUrl','http://localhost:3001');

// Mongo Section
var mongoConfig = require(path.join('lib', 'modules', 'Config')).getConfig();
mongoConfig.load({
  mongoAddr: ('127.0.0.1:27017'),
  mongoDb: 'samsung',
  mongoRs: (process.env.MONGO_RS || '')
});

var MongoDbFactory = require(path.join('lib', 'modules', 'MongoDbFactory'));
var mongoDbFactory = new MongoDbFactory();
var mongoDb = mongoDbFactory.getDb(mongoSkin, mongoConfig);
var StoreFactory = require(path.join('lib', 'modules', 'StoreFactory'));
var storeFactory = new StoreFactory();
var store = storeFactory.getStore(Q, async, _, mongoDb);

// 
var validator = require(path.join('lib', 'modules', 'Validator'))(Q, _).getInstance();
var utils = require(path.join('lib', 'modules' ,'Utils'))(path, Q, crypto, mongoSkin).getInstance();
var auth = require(path.join('lib', 'modules' ,'Auth'))(Q, async, moment, uuid, validator,store,utils,crypto, passport).getInstance();
														
// Route Section
var routeAuth = require(path.join('routes', 'Auth'))(Q, async, moment, uuid, validator, store, utils, crypto, passport).getInstance();
var routeUser = require(path.join('routes', 'User'))(moment, uuid, crypto, validator, store, utils, auth, _).getInstance();
var routeQuestions = require(path.join('routes', 'Questions'))(moment, uuid, _, validator, store, utils, auth, async, Q).getInstance();

// Server Section
var app = require(path.join('api'))(path, express, session, passport,cookieParser,bodyParser,config, routeUser, routeQuestions, routeAuth, PassportLocalStrategy).getInstance();

http.createServer(app).listen(app.get('port'), function(){
	console.log('App listens port '+ app.get('port'));
});

