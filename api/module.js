var Api = function(path, express, session, passport ,cookieParser, bodyParser,config, routeUser, routeQuestions, routeAuth, PassportLocalStrategy){

	var instance;

	function init () {

		var app = express();

		app.set('port',3001);
		app.use(cookieParser());
		app.use(bodyParser.json());
		app.use(express.static(path.join(config.get('rootPath'), 'public'), {index: 'index.html'}));
		app.use('/images', express.static(path.join(__dirname, '..', '..', 'images')));

		app.use(session({ 
			secret: 'Nastenka',
			resave: true,
    		saveUninitialized: true
  		}));

		app.use(passport.initialize());
		app.use(passport.session());


		passport.serializeUser(routeAuth.passportSerializeUser);
		passport.deserializeUser(routeAuth.passportDeserializeUser);
		passport.use('local', new PassportLocalStrategy({
		  usernameField: 'userName',
		  passwordField: 'password'
		}, routeAuth.passportStrategy));

		// User endpoints
		app.get('/manage/user/', routeAuth.ensureAuthenticated, routeUser.query);
		app.post('/manage/user/', routeAuth.ensureAuthenticated, routeUser.post);
		app.get('/manage/user/:id', routeAuth.ensureAuthenticated, routeUser.get);
		app.delete('/manage/user/:id', routeAuth.ensureAuthenticated, routeUser.delete);
		app.post('/manage/user/login', routeAuth.login);
		app.get('/manage/user/logout', routeAuth.logout);
	
	  	//-- Unapproved Questions endpoints
	  	app.post('/manage/unapproved/', routeAuth.ensureAuthenticated, routeQuestions.postUnapprovedQuestion);
	  	app.get('/manage/unapproved/collection/:collection', routeAuth.ensureAuthenticated, routeQuestions.query);
	  	app.get('/manage/unapproved/:id/:collection', routeAuth.ensureAuthenticated, routeQuestions.get);
	  	app.delete('/manage/unapproved/:id/:collection', routeAuth.ensureAuthenticated, routeQuestions.delete);
		
	  	//-- Approved Questions endpoints

	  	app.post('/manage/approved/', routeAuth.ensureAuthenticated, routeQuestions.postApprovedQuestion);
	  	app.get('/manage/approved/collection/:collection', routeAuth.ensureAuthenticated, routeQuestions.query);
	  	app.get('/manage/approved/:id/:collection', routeAuth.ensureAuthenticated, routeQuestions.get);
	  	app.delete('/manage/approved/:id/:collection', routeAuth.ensureAuthenticated, routeQuestions.delete);

	  	// -- Ranking
	  	app.get('/manage/ranking', routeAuth.ensureAuthenticated, routeQuestions.getRanking);

		return app;
	};

	return {
		getInstance: function (){
			if (!instance) {
				instance = init();
			}
			return instance;
		}
	};

};

module.exports = exports = Api;