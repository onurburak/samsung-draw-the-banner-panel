var RouteAuth = function (Q, async, moment, uuid, validator, store, utils, crypto, passport) {

  var instance;

  function init() {

    return {

    	passportSerializeUser: function(user, done) {
			  done(null, user.userName);
			},

		passportDeserializeUser: function(userName, done) {

		  var query = {userName: userName, status: {$nin : ["DELETED", "HOLD"]}};
		  store.findOne('users', query).then(function(user){

			if(!user)
			{
			  //res.send({code:90, message:'FAIL_AUTH'});
			  throw new Error('FAIL_AUTH');
			}

			done(null, user);

		  }).fail(function(error){

			done(null, false, {message: 'We think, we do not know you'});

		  });

		},

		passportStrategy: function(userName, password, done) {

		  process.nextTick(function () {
	  		var query = {userName: userName, status: {'$ne':'DELETED'}};
			store.findOne('users', query).then(function(user){
				if(!user)
					throw new Error('FAIL_AUTH');

				if(user.password != crypto.createHash('sha1').update(password).digest('hex')/*instance.encrypt(password)*/)
					throw new Error('FAIL_AUTH');

				if(user.status === 'HOLD')
					throw new Error('FAIL_AUTH_HOLD');

			  	done(null, user);

			}).fail(function(error){

			  	done(null, false, {message: 'We think, we do not know you'});

			});

		  });
		},

		ensureAuthenticated: function(req, res, next) {
			// if(process.env.SEAMLESS_ENV != 'prod')
			// return next();
		  if (req.isAuthenticated()) { return next(); }
		  res.send({code:90, message:'FAIL_SESSION'});
		  return;
		},

		login: function(req, res, next) {
	
		  passport.authenticate('local', function(err, user, info) {

			if (err) { return next(err); }
			
			if (!user) {
			  res.send({code:99, message:'FAIL_AUTH'});
			  return;
			}

			req.logIn(user, function(err) {
			  if (err) { return next(err); }
			  return res.send({code:100, message:'SUCCESS', user:user});
			});

		  })(req, res, next);

		},

		logout: function(req, res){
		  req.logout();
		  res.send({code:100, message:'SUCCESS'});
		  return;
		}
      
    };
  }

  return {

    getInstance: function () {

      if (!instance) {
        instance = init();
      }

      return instance;
    }

  };

};

module.exports = exports = RouteAuth;
