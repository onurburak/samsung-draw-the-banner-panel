var RouteUser = function(moment, uuid, crypto, validator, store, utils, auth, _){

  var instance;

  function init() {

    var isEmail = function (email) {
      //return /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/.test( email );
      //return /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/.test(email);
      return /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/.test(email);
    };

    return {

      query: function (req, res) {
       
        var query = {};
        var options = {
          'sort': {
            'name': 1
          }
        };

        store.find('users', query, options).then(function (users) {

          res.send({code: 100, message: 'SUCCESS', data: users});
          return;

        }).fail(function (error) {

          res.send({code: 500, message: 'FAIL_SYSTEM', data: error});
          return;

        });
      
      },

      get: function (req, res) {
        
        var query = {'_id': utils.toObjectID(req.params.id)};
        
        store.findOne('users', query).then(function (user) {
          
          res.send({code: 100, message: 'SUCCESS', data: user});

        }).fail(function (error) {

          if (error.stack)
            console.log(error.stack)

          res.send({code: 500, message: 'FAIL_SYSTEM', data: error.message});
          return;

        });
      },

      post: function(req, res) {

        var now = moment().unix();
        var _user;
        var rules;
        
        rules = {
          'userName': {
            'required': 'User Name is required',
            'length(2,50)': 'User Name should contain 2-50 characters.'
          },
          'eMail': {
            'required': 'E mail is required',
            'length(2,50)': 'E mail should contain 2-50 characters.'
          }
        };
        

        validator.validate(req.body, rules).then(function(valResult) {
          if (valResult !== true) {
            throw {code: 99, message: 'FAIL_VALIDATION', data: valResult};
            return;
          }


          if(req.body._id)
          {
            return store.findOne('users', {_id: utils.toObjectID(req.body._id)});
          }
          else
          {
            _user = {
              time_created: now
            };

            return _user;
          }

        }).then(function(user){
         
          _user = _.clone(user);
          _user.eMail = req.body.eMail;
          _user.userName = req.body.userName;
          _user.time_updated = now;
          _user.role = req.body.role;
          _user.status = 'ACTIVE';
           
          //password
          if (req.body.newPassword){
            _user.password = crypto.createHash('sha1').update(req.body.newPassword).digest('hex');
          } else{
            if (!_user.password){
              _user.password = (req.body.password.length < 31 ? crypto.createHash('sha1').update(req.body.password).digest('hex') : user.password);
            }
          }
            

          return _user;

        }).then(function(_user){

          if(req.body._id) {
            var query = {'_id': utils.toObjectID(req.body._id)};
            return store.findOne('users', query);
          }

          return null;

        }).then(function(user){
          
          if (user)
            return store.update('users',{'_id': utils.toObjectID(user._id)},_user);
          else
            return store.save('users', _user);

        }).then(function(user){
          res.send({code:100, message:'SUCCESS', data:user});
          return;

        }).fail(function(error){
            res.send({code:error.code, message:error.message, data:error.data});
            return;

        });

      },

      delete: function(req, res){

        var query = { _id: utils.toObjectID(req.params.id)};
       
          store.findOne('users', query).then(function(user){
            if(user){
              user.status = 'DELETED';
              return store.save('users', user);
            }
            else{
              throw {code:99, message:'FAIL_AGENCY_MISMATCH', data:false};
              return;
            }

          }).then(function(user){

            res.send({code:100, message:'SUCCESS', data:user});
            return;

          }).fail(function(error){

            res.send({code:500, message:'FAIL_SYSTEM', data:error});
            return;

          });

      }

    };
  }



  return {

    getInstance: function () {

      if ( !instance ) {
        instance = init();
      }

      return instance;
    }

  };

};

module.exports = exports = RouteUser;
