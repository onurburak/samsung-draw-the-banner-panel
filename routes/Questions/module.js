var RouteQuestions = function(moment, uuid, _, validator, store, utils, auth, async, Q){

  var instance;

  function init() {

    return {

      //-- approved questions routes

      query: function (req, res) {
      
        var query = {'status': {'$ne':'DELETED'}};
        var options = {
          'sort': {
            'time_created': -1
          }
        };
        var collectionName = (req.params.collection == "approved" ? "approvedQuestions" : "unapprovedQuestions");


        // if (req.user.role != 'admin') {
        //   throw {code: 99, message: 'PERMISSION_ERROR', data: valResult};
        //   return;
        // }

        store.find(collectionName, query, options).then(function(questions){

          res.send({code:100, message:'SUCCESS', data: questions});
          return;

        }).fail(function(error){

          if(error.stack)
          {
            console.log(error.stack);
            res.send({code:500, message:'FAIL_SYSTEM', data:error.message});
            return;
          }

          res.send({code:500, message:'FAIL_SYSTEM', data:error});

          return;
        });

      },

      get: function (req, res) {

        var collectionName = (req.params.collection == "approved" ? "approvedQuestions" : "unapprovedQuestions");
        
        validator.validate(req.params, {
          'id': {
            'required': 'FAIL_REQUIRED',
            'length(2,50)': 'FAIL_LENGTH'
          }
        }).then(function(valResult){

          if(valResult !== true)
          {
            throw {code:99, message:'FAIL_VALIDATION', data: valResult};
            return;
          }

          var query = {'_id': utils.toObjectID(req.params.id)};
          return query;

        }).then(function(query){

          return store.findOne(collectionName, query);

        }).then(function (question){

          res.send({code:100, message:'SUCCESS', data:question});

        }).fail(function(error){

          if(error.stack)
          {
            console.log(error.stack);
            res.send({code:500, message:'FAIL_SYSTEM', data:error.message});
            return;
          }

          res.send(error);
          return;

        });

      },

      delete: function (req ,res) {
        
        var collectionName = (req.params.collection == "approved" ? "approvedQuestions" : "unapprovedQuestions");
        var now = moment().unix();
        var query = {'_id': utils.toObjectID(req.params.id)};
        
        store.findOne(collectionName, query).then(function (question){

          if(!question)
          {
            throw {code:303, message:'FAIL_NOT_FOUND'};
            return;
          }

          question.status = 'DELETED';
          
          return store.save(collectionName, question);

        }).then(function (question){

          res.send({code:100, message:'SUCCESS', data:question});
          return;

        }).fail(function(error){

          if(error.stack)
          {
            console.log(error.stack);
            res.send({code:500, message:'FAIL_SYSTEM', data:error.message});
            return;
          }

          res.send(error);
          return;

        });    

      },

      postApprovedQuestion: function (req ,res) {
       
        var now = moment().unix();
        
        var _question;

        validator.validate(req.body, {
          
        }).then(function(valResult) {

          if (valResult !== true) {
            throw {code: 99, message: 'ERROR', data: valResult};
            return;
          }
          
          if (req.user.role != 'admin') {
            throw {code: 99, message: 'PERMISSION_ERROR', data: valResult};
            return;
          }

        }).then(function(){

          if (req.body.status != "UNAPPROVED") {
            var query = {'_id': utils.toObjectID(req.body._id)};
            return store.findOne('approvedQuestions', query);
          }
          else {
            var question = {
              time_created: now,
              status: 'APPROVED'
            };

            return question;
          }
          
        }).then(function(question) {
          _question = question;
          
          question.fbID = req.body.fbID;
          question.photoPath = req.body.photoPath;
          question.shape = req.body.shape;
          question.alternativeOption = req.body.alternativeOption;
          question.impression = (req.body.impression ? req.body.impression : 0);
          question.score = (req.body.score ? req.body.score : 0);
          question.status = "APPROVED";
          
          return question;

        }).then(function(question){

          return store.save('approvedQuestions', question);

        }).then(function (){

          //--check existence of user
          return store.findOne('audience', {'fbID': req.body.fbID});

        }).then(function (existUser) {
          
          if (existUser == null) {

            var user = {
                fbID: req.body.fbID,
                name: req.body.name,
                fbPhoto: req.body.fbPhoto,
                msg: req.body.msg,
                score: 0,
                impression: 0
            };

            return user;
          }

          return null;

        }).then(function (user) {

          //-- check existence of user
          if (user != null) {
            return store.save('audience', user);  
          }

        }).then(function () {
          
          //-- remove unapproved question
          var _unapprovedQuestion = {
            fbID: req.body.fbID,
            photoPath: req.body.photoPath,
            shape: req.body.shape,
            status: "DELETED",
            name: req.body.name,
            fbPhoto: req.body.fbPhoto,
            msg: req.body.msg
          };

          return store.update('unapprovedQuestions', {'_id': utils.toObjectID(req.body._id)}, _unapprovedQuestion);

        }).then(function(){

          res.send({code:100, message:'SUCCESS', data:{}});
          return;

        }).fail(function(error){

          if(error.stack)
          {
            console.log(error.message);
            console.log(error.stack);
            res.send({code:500, message:'FAIL_SYSTEM', data:error.message});
            return;
          }

          res.send(error);

          return;

        });

      },

      postUnapprovedQuestion: function (req, res) {
        
        var now = moment().unix();
        
        var _question;

        validator.validate(req.body, {
          'fbID': {
            'required': 'FAIL_REQUIRED',
            'length(2,50)': 'FAIL_LENGTH'
          }
        }).then(function(valResult) {

          if (valResult !== true) {
            throw {code: 99, message: 'ERROR', data: valResult};
            return;
          }
          
          if (req.user.role != 'admin') {
            throw {code: 99, message: 'PERMISSION_ERROR', data: valResult};
            return;
          }

        }).then(function(){

          if (req.body._id) {
            var query = {'_id': utils.toObjectID(req.body._id)};
            return store.findOne('unapprovedQuestions', query);
          }
          else {
            var question = {
              time_created: now,
              status: 'ACTIVE'
            };

            return question;
          }
          
        }).then(function(question) {
          _question = question;
         
          question.fbID = req.body.fbID;
          question.photoPath = req.body.photoPath;
          question.shape = req.body.shape;
          question.status = "ACTIVE";
          
          return question;

        }).then(function(question){

          return store.save('unapprovedQuestions', question);

        }).then(function(question){

          res.send({code:100, message:'SUCCESS', data:question});
          return;

        }).fail(function(error){

          if(error.stack)
          {
            console.log(error.message);
            console.log(error.stack);
            res.send({code:500, message:'FAIL_SYSTEM', data:error.message});
            return;
          }

          res.send(error);

          return;

        });

      },

      getRanking: function (req, res) {
        
        var options = {
          'sort': {'score': -1},
          'limit': 20
        };

        store.find('audience', {}, options).then(function (users) {

          res.send({code: 100, message: 'SUCCESS', data: users});
          return;

        }).fail(function (error) {

          res.send({code: 500, message: 'FAIL_SYSTEM', data: error});
          return;

        });

      }


    };
  }

  return {

    getInstance: function () {

      if ( !instance ) {
        instance = init();
      }

      return instance;
    }

  };

};

module.exports = exports = RouteQuestions;
