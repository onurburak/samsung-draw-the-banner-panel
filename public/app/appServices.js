var panelServices = angular.module('panelServices', ['ngResource']);

var User = function($resource){

  return $resource('/manage/user/', {}, {
    query: {method:'GET', params:{}, isArray:false},
    save: {method:'POST', params:{}, isArray:false},
    remove: {method:'DELETE', params:{}, isArray:false},
    get: {url:'/manage/user/:id', method:'GET', params:{'id': '@id'}, isArray:false},
    login: {url: '/manage/user/login', method:'POST', params:{}, isArray:false},
    logout: {url: '/manage/user/logout', method:'GET', params:{}, isArray:false}
  });

}; 


var ApprovedQuestion = function ($resource) {

	return $resource('/manage/approved/',{},{
		query: {url:'/manage/approved/collection/:collection', method:'GET', params:{'collection':'@collection'}, isArray:false},
		save: {method:'POST', params:{}, isArray:false},
    get: {url:'/manage/approved/:id/:collection', method:'GET', params:{'id':'@id', 'collection': '@collection'}, isArray:false},
    remove: {method: 'DELETE', params:{}, isArray:false}
	});
};

var UnapprovedQuestion = function ($resource) {

  return $resource('/manage/unapproved/',{},{
    query: {url:'/manage/unapproved/collection/:collection', method:'GET', params:{'collection':'@collection'}, isArray:false},
    save: {method:'POST', params:{}, isArray:false},
    get: {url:'/manage/unapproved/:id/:collection', method:'GET', params:{'id': '@id', 'collection': '@collection'}, isArray:false},
    send: {url:'/manage/unapproved/:id', method:'POST', params:{'id':'@id'}, isArray:false},
    remove: {method:'DELETE', params:{}, isArray:false}
  });
};


panelServices.factory('User', ['$resource', User]);
panelServices.factory('UnapprovedQuestion', ['$resource', UnapprovedQuestion]);
panelServices.factory('ApprovedQuestion', ['$resource', ApprovedQuestion]);
