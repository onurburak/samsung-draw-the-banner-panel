var panelControllers = angular.module('panelControllers', []);

panelControllers.controller('NavBarController', ['$scope', '$rootScope', '$http', '$location', '$localStorage', 'User', 'toastr',

	function($scope, $rootScope, $http, $location, $localStorage, User, toastr) {
		//Popup content
		$rootScope.setPopupContent = function(title, body, cBText, notification) {
			//popup content object
			$rootScope.popupContent = {
				title: title,
				body: body,
				confirmButtonText: cBText
			};
			//popup notification object
			if (notification) {
				var newNot = _.cloneDeep(notification);
				newNot.sendTime = moment.unix(newNot.sendTime).format('DD/MM/YYYY HH:mm:ss');
				$rootScope.notificationToSend = newNot;
			}


		};
		// set remote action parameters
		$rootScope.setRemoteActionParameters = function(method, url, id, collection) {

			if (id != "") {
				url += id;
			}
			if (collection != "") {url += "/" + collection};

			$rootScope.remoteActionParameters = {
				method: method,
				url: url
			};
		};
		// for remote action
		$rootScope.remoteAction = function(obj) {

			var req = {
				method: obj.method,
				url: obj.url
			};

			$http(req).success(function(data, status) {

				if (data.code == 100) {
					toastr.success('', 'Success', {
						positionClass: 'toast-bottom-right'
					});
				} else {
					if (data.message == 'FAIL_SYSTEM_NOTIFICATION') {
						
						toastr.error(data.data.error, 'Error', {
							positionClass: 'toast-bottom-right'
						});
					} else {
						toastr.error('Oppps something went wrong!', 'Error', {
							positionClass: 'toast-bottom-right'
						});
					}
				}

				$rootScope.$broadcast('refreshLoad');

			}).error(function(data, status) {
				console.log('data: ' + data + 'status: ' + status);
				toastr.error('Oppps something went wrong!', 'Error', {
					positionClass: 'toast-bottom-right'
				});
			});
		};
		// go back in history method
		$rootScope.GoBack = function() {
			window.history.back();
		};
		//Form Validator method
		$rootScope.validator = {
			//required
			Required: function(value) {
					if (typeof value == 'undefined' || !value || value.trim() == '')
						return false;

					return true;
				}
				//
		};


		// User Auth
		$rootScope.isAdmin = false;
		$rootScope.isPublisher = false;

		$scope.$on('$locationChangeSuccess', function(event) {

			if ($localStorage.currentUser) {

				User.get({
					'id': $localStorage.currentUser._id
				}, function(response) {

					if (response.code == 100) {
						delete $localStorage.currentUser;
						$localStorage.currentUser = response.data;
						$scope.isNavbarEnabled = true;

						if ($localStorage.currentUser.role === 'admin') {
							$rootScope.isAdmin = true;
						}

						$scope.isAdmin = ($localStorage.currentUser.role === 'admin');
					}
				});
			} else {
				User.logout();
				$scope.isNavbarEnabled = false;
				$location.path('/user/login');
				return;
			}

			$rootScope.isPublisher = false;
			$rootScope.isPublisher = ($localStorage.currentUser.role === 'publisher');

		});

		$scope.logout = function() {

			$scope.isPublisher = false;
			$scope.isAdmin = false;
			$scope.isNavbarEnabled = false;
			delete $localStorage.currentUser;
			$location.path('/user/login');
		}


	}
]);

panelControllers.controller('UserListController', ['$scope', '$filter', '$rootScope', '$routeParams', '$http', '$location', 'User', 'toastr',

	function($scope, $filter, $rootScope, $routeParams, $http, $location, User, toastr) {

		$scope.setPopupContent = $rootScope.setPopupContent;

		$scope.load = function() {

			User.query(function(response) {
				if (response.code == 100) {
					$scope.userList = response.data;
				} else {
					console.log('Oppps something went wrong!');
					toastr.error('Oppps something went wrong! Try Again.', 'Error', {
						positionClass: 'toast-bottom-right'
					});
				}

			});

		};

		// To refresh load from outside
		$scope.$on('refreshLoad', function() {
			$scope.load();
		});

		$scope.load();

	}
]);

panelControllers.controller('NewUserController', ['$scope', '$filter', '$rootScope', '$routeParams', '$http', '$location', 'User', 'toastr',

	function($scope, $filter, $rootScope, $routeParams, $http, $location, User, toastr) {


		$scope.load = function() {

			if ($routeParams.id) {

				User.get({
					'id': $routeParams.id
				}, function(response) {
					if (response.code == 100) {
						$scope.newUser = response.data;
						$scope.userAppList = response.data.apps;
						$scope.isNewPassword = true;
					} else {
						console.log(' Oppps something went wrong!');
						toastr.error('Oppps something went wrong! Try Again.', 'Error', {
							positionClass: 'toast-bottom-right'
						});
					}
				});

			} else {
				$scope.isNewPassword = false;
			}


		};


		$scope.save = function() {
			//Requirement check
			if (!$rootScope.validator.Required($scope.newUser.userName) ||
				!$rootScope.validator.Required($scope.newUser.eMail)) {
				toastr.warning("All properties are required!", "Warning", {
					positionClass: 'toast-bottom-right'
				});
				return;
			}

			//Check part
			if ($scope.newUser.password == undefined || $scope.newUser.password == "") {
				toastr.error('Password must has at least 6 characters', 'Error', {
					positionClass: 'toast-bottom-right'
				});
				return;
			} else if ($scope.newUser.password.length < 6) {
				toastr.error('Password must has at least 6 characters', 'Error', {
					positionClass: 'toast-bottom-right'
				});
				return;
			} else if ($scope.newUser.password.length > 0 && $scope.newUser.newPassword && $scope.newUser.newPassword.length < 6) {

				toastr.error('Password must has at least 6 characters', 'Error', {
					positionClass: 'toast-bottom-right'
				});
				return;
			}


			// Save user to db
			User.save($scope.newUser, function(response) {

				if (response.code == 100) {
					toastr.success('Saved successfully!', 'Success', {
						positionClass: 'toast-bottom-right'
					});
					$location.path('/user/list');
				} else {
					toastr.error(response.data[Object.keys(response.data)[0]], 'Error', {
						positionClass: 'toast-bottom-right'
					});
					$scope.newUser.password = "";
				}
			});

		};



		$scope.load();

	}
]);

panelControllers.controller('UserLoginController', ['$scope', '$filter', '$rootScope', '$routeParams', '$http', '$location', 'User', '$localStorage', 'toastr',
	function($scope, $filter, $rootScope, $routeParams, $http, $location, User, $localStorage, toastr) {


		$scope.$storage = $localStorage.$default({
			currentUser: ''
		});
		$scope.isPublisher = false;
		$scope.isAdmin = false;
		$scope.isNavbarEnabled = false;
		$scope.user = null;

		function cookieTimeOrganizer(days) {

			var d = new Date();
			if (days < 0) d.setTime(d.getTime() - (days * 24 * 60 * 60 * 1000));
			else d.setTime(d.getTime() + (days * 24 * 60 * 60 * 1000));
			var expires = "expires=" + d.toUTCString();
			return expires;
		};

		function getCookie(cname) {

			var name = cname + "=";
			var ca = document.cookie.split(';');
			for (var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') c = c.substring(1);
				if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
			}
			return "";
		}

		function setCookie(cname, cpass, exdays) {

			var expires = cookieTimeOrganizer(exdays);
			document.cookie = 'pu=' + cname + ';' + expires;
			document.cookie = 'pp=' + cpass + ';' + expires;
		}

		function deleteCookie(cname) {

			var expires = cookieTimeOrganizer(-10000);
			document.cookie = cname + '=' + '' + ';' + expires;
		}

		$scope.login = function() {

			User.login($scope.user, function(response) {
				if (response.code === 99) {
					toastr.error('I think we have not met before.', 'Error', {
						positionClass: 'toast-bottom-right'
					});
					$scope.validation = response.data;
				} else {
					delete $scope.$storage.currentUser;
					$scope.$storage.currentUser = response.user;

					// Remember me
					if ($scope.isRememberMe) {
						setCookie($scope.user.userName, $scope.user.password, 3);
					} else {
						deleteCookie('pu');
						deleteCookie('pp');
					}

					$location.path('/unapproved/list/'); // go to main page
				}

			});
		};


		$scope.load = function() {
			// authentication 
			var cookieUserName = getCookie("pu"),
				cookiePassword = getCookie("pp");
			//check cookies	
			if (cookieUserName && cookiePassword) {
				$scope.user = {};
				$scope.isRememberMe = true;
				$scope.user.userName = cookieUserName;
				$scope.user.password = cookiePassword;
			}

		};

		$scope.load();

	}
]);

panelControllers.controller('UnapprovedListController', ['$scope', '$filter', '$rootScope', '$routeParams', '$http', '$location', 'UnapprovedQuestion', '$localStorage', 'toastr',

	function($scope, $filter, $rootScope, $routeParams, $http, $location, UnapprovedQuestion, $localStorage, toastr) {


		$scope.load = function() {
			
			UnapprovedQuestion.query({collection: "unapproved"},function(response) {
				if (response.code == 100) {
					$scope.questions = response.data;
				} else {
					console.log('Oppps something went wrong!');
				}
			});

		};

		// To refresh load from outside
		$scope.$on('refreshLoad', function() {
			$scope.load();
		});

		$scope.load();

	}
]);

panelControllers.controller('NewUnapprovedController', ['$scope', '$filter', '$rootScope', '$routeParams', '$http', '$location', 'UnapprovedQuestion', 'ApprovedQuestion', 'toastr',

	function($scope, $filter, $rootScope, $routeParams, $http, $location, UnapprovedQuestion, ApprovedQuestion, toastr) {


		// -- onload

		$scope.load = function() {

			if ($routeParams.id) {

				UnapprovedQuestion.get({
					'id': $routeParams.id,
					'collection': 'unapproved'
				}, function(response) {
					if (response.code == 100) {
						
						$scope._question = response.data;

					} else {
						console.log('Oppps something went wrong!');
					}
				});
			} 
		};

		// -- approce
		$scope.approve = function () {
			
			$scope._question.alternativeOption = $scope.alternativeOption;

			ApprovedQuestion.save($scope._question, function (response) {

				if (response.code == 100) {

					toastr.success('Saved successfully!', 'Success', {
						positionClass: 'toast-bottom-right'
					});
					$location.path('/unapproved/list/');

				} else {
					console.log('Oppps something went wrong!');
					toastr.error('Oppps something went wrong! Try Again.', 'Error', {
						positionClass: 'toast-bottom-right'
					});
				}
			});

		}

		$scope.load();

	}
]);










panelControllers.controller('ApprovedListController', ['$scope', '$filter', '$rootScope', '$routeParams', '$http', '$location', 'ApprovedQuestion', '$localStorage', 'toastr',

	function($scope, $filter, $rootScope, $routeParams, $http, $location, ApprovedQuestion, $localStorage, toastr) {


		$scope.load = function() {
			
			ApprovedQuestion.query({collection: "approved"},function(response) {
				
				if (response.code == 100) {
					$scope.questions = response.data;
				} else {
					console.log('Oppps something went wrong!');
				}
			});

		};

		// To refresh load from outside
		$scope.$on('refreshLoad', function() {
			$scope.load();
		});

		$scope.load();

	}
]);

panelControllers.controller('NewApprovedController', ['$scope', '$filter', '$rootScope', '$routeParams', '$http', '$location', 'ApprovedQuestion', 'toastr',

	function($scope, $filter, $rootScope, $routeParams, $http, $location, ApprovedQuestion, toastr) {


		// -- onload

		$scope.load = function() {

			if ($routeParams.id) {

				ApprovedQuestion.get({
					'id': $routeParams.id,
					'collection': 'approved'
				}, function(response) {
					if (response.code == 100) {
						
						$scope._question = response.data;

					} else {
						console.log('Oppps something went wrong!');
					}
				});
			} 
		};

		// -- approce
		$scope.approve = function () {
			
			ApprovedQuestion.save($scope._question, function (response) {

				if (response.code == 100) {

					toastr.success('Saved successfully!', 'Success', {
						positionClass: 'toast-bottom-right'
					});
					$location.path('/unapproved/list/');

				} else {
					console.log('Oppps something went wrong!');
					toastr.error('Oppps something went wrong! Try Again.', 'Error', {
						positionClass: 'toast-bottom-right'
					});
				}
			});

		}

		$scope.load();

	}
]);


panelControllers.controller('RankingController', ['$scope', '$filter', '$rootScope', '$routeParams', '$http', '$location', 'toastr',

	function($scope, $filter, $rootScope, $routeParams, $http, $location, toastr) {


		// -- onload
		$scope.load = function() {

			$http.get('/manage/ranking').
			  then(function(response) {
			  	console.log(response);
			   $scope.ranking = response.data.data;
			  });

			
		};

		$scope.load();

	}
]);



