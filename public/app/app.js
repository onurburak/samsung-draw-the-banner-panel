var PushApp = angular.module('PushApp', [
	'ngRoute', 
  'ngStorage',
  // 'ngAnimate',
  'toastr',
  'panelControllers',
  'panelServices'

]);

PushApp.config(['$httpProvider',function($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
}]);

PushApp.factory('authInterceptor',['$q','$location',function($q, $location){
  return {
    response: function (response) {

			if(response.data.code === 90)
			{
        var redirectUrl = '/user/login';
				$location.path(redirectUrl);
				return $q.reject(response);
			}

      return response;
    },
    responseError: function (response) {
			var redirectUrl = '/user/login';
      $location.path(redirectUrl);
			return $q.reject(response);
    }
  };
}]);


PushApp.filter('range', function() {
  return function(input, total) {
    total = parseInt(total);
    for (var i=0; i<total; i++){
      input.push(i);
    }
    return input;
  };
});

PushApp.run(function($rootScope, $location) {
    $rootScope.location = $location;
});

PushApp.config(['$routeProvider',
	function($routeProvider) {
    	$routeProvider.when('/user/list', {
          templateUrl: 'templates/user-list.html',
          controller: 'UserListController'
        }).when('/user/new', {
          templateUrl: 'templates/user-form.html',
          controller: 'NewUserController'
        }).when('/user/login', {
          templateUrl: 'templates/user-login.html',
          controller: 'UserLoginController'
        }).when('/user/:id', {
          templateUrl: 'templates/user-form.html',
          controller: 'NewUserController'
        })

        .when('/unapproved/list', {
          templateUrl: 'templates/unapproved-list.html',
          controller: 'UnapprovedListController'
        }).when('/unapproved/new', {
          templateUrl: 'templates/unapproved-form.html',
          controller: 'NewUnapprovedController'
        }).when('/unapproved/:id', {
          templateUrl: 'templates/unapproved-form.html',
          controller: 'NewUnapprovedController'
        })


        .when('/approved/list', {
          templateUrl: 'templates/approved-list.html',
          controller: 'ApprovedListController'
        }).when('/approved/new', {
          templateUrl: 'templates/approved-form.html',
          controller: 'NewApprovedController'
        }).when('/approved/:id', {
          templateUrl: 'templates/approved-form.html',
          controller: 'NewApprovedController'
        })

        .when('/ranking', {
          templateUrl: 'templates/ranking.html',
          controller: 'RankingController'
        })

        .otherwise({
        	redirectTo: '/unapproved/list'
          
      	});
  	}]);