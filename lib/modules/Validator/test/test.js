var path = require('path');
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var should = chai.should();
var expect = chai.expect;

require('rootpath')();

var Q = require('q'),
    _ = require('underscore');

describe('Validator', function(){

  describe('#validate', function(){

    it('should be a function', function(){
      var validator = require(path.join('modules', 'Validator'))(Q, _).getInstance();
      validator.validate.should.be.a('function');
    });

    it('should throw error when no store object is specified', function(){
      var validator = require(path.join('modules', 'Validator'))(Q, _).getInstance();
      return validator.validate().should.be.rejectedWith(Error);
    });

    it('should throw error when no rules object is specified', function(){
      var validator = require(path.join('modules', 'Validator'))(Q, _).getInstance();
      return validator.validate({}).should.be.rejectedWith(Error);
    });

    it('should throw error when invalid rule keys are present in rules object', function(){
      var validator = require(path.join('modules', 'Validator'))(Q, _).getInstance();
      return validator.validate({}, {'mock':{'mockValidate':'mockError'}}).should.be.rejectedWith(Error);
    });

    it('should return true if store validates against rules', function(){
      var validator = require(path.join('modules', 'Validator'))(Q, _).getInstance();
      return validator.validate({'mock':'mock'}, {'mock':{'required':'FAIL'}}).should.become(true);
    });

    it('should return error object if store does not validate against rules', function(){
      var validator = require(path.join('modules', 'Validator'))(Q, _).getInstance();
      return validator.validate({'mock':''}, {'mock':{'required':'FAIL'}}).should.eventually.be.an('object');
    });

    it('sould return error object if validateRegex not satisfied', function(){
      var validator = require(path.join('modules', 'Validator'))(Q, _).getInstance();
      return validator.validate({'mock':'asdfghjkl'}, {'mock': {'regex(^[0-9]+$)':'FAIL_REGEX'}}).should.eventually.be.an('object');
    });

    it('sould return error object if validateOneOf not satisfied', function(){
      var validator = require(path.join('modules', 'Validator'))(Q, _).getInstance();
      return validator.validate({'mock':'valueInvalid'}, {'mock': {'oneOf(value1, value2, value3)':'FAIL_ONE_OF'}}).should.eventually.be.an('object');
    });

  });
});
