var Validator = function(Q, _){

  var instance;

  function init() {

    var validateRequired = function(key, value, opts, msg){

  		if(typeof value == 'undefined' || !value || value.trim() == '')
  		{
  			return false;
  		}

  		return true;
  	};

  	var validateLength = function(key, value, opts, msg){

  		if(typeof value == 'undefined' || !value)
  			return true;

  		if(opts.min && value.length < opts.min)
  		{
  			return false;
  		}

  		if(opts.max && value.length > opts.max)
  		{
  			return false;
  		}

  		return true;

  	};

  	var validateEmail = function(key, value, opts, msg){

  		if(typeof value == 'undefined')
  			return true;

  		if (!value.match(emailRegex))
  		{
  			return false;
      }

  		return true;
  	};

  	var validateInteger = function(key, value, opts, msg){

  		if(typeof value == 'undefined' || !value || value.trim() == '')
  			return true;

  		if (!value.match(/^[0-9]+$/))
  		{
  			return false;
      }

  		return true;
  	};

    var validateGreater = function(key, value, opts, msg){

      if(typeof value == 'undefined' || !value || value.trim() == '')
        return true;

      if (value.match(/^[0-9]+$/))
      {
        return parseInt(value) > opts.than;
      }
      else if (value.match(/^[0-9]+\.[0-9]+$/))
      {
        return parseFloat(value) > opts.than;
      }
      else
      {
        return false;
      }

      return true;

    };

    var validateRegex = function(key, value, opts, msg){

      if(typeof value == 'undefined' || !value || value.trim() == '')
        return true;

      if(!value.match(opts.pattern))
      {
        return false;
      }

      return true;

    };

    var validateOneOf = function(key, value, opts, msg){

      if(typeof value == 'undefined' || !value || value.trim() == '')
        return true;

      if(opts.values.indexOf(value) == -1)
      {
        return false;
      }

      return true;

    };

    var validateSpace = function(key,value,opts,msg){
      if (value.indexOf(" ") == -1) {
        return true;
      }

      return false;
    }


  	// FILE VALIDATION

  	var validateFileRequired = function(key, value, opts, msg){

  		if(typeof value == 'undefined' || !value || value['size'] == 0)
  		{
  			return false;
  		}

  		return true;

  	};

  	var validateFileType = function(key, value, opts, msg){

  		if(typeof value == 'undefined' || !value || value['size'] == 0)
  			return true;

  		if(opts.types.indexOf(value['type']) == -1)
  		{
  			return false;
  		}

  		return true;

  	};

  	var validateFileSizeMax = function(key, value, opts, msg){

  		if(typeof value == 'undefined' || !value || value['size'] == 0)
  			return true;

  		if(value['size'] > opts.max)
  		{
  			return false;
  		}

  		return true;

  	};

    return {

      validate: function (store, rules) {

        var deferred = Q.defer();

        if(!store || typeof store != 'object')
        {
          deferred.reject(new Error('store object not provided'));
          return deferred.promise;
        }

        if(!rules || typeof rules != 'object')
        {
          deferred.reject(new Error('rules object not provided'));
          return deferred.promise;
        }

        var error = {};
        var result;

        for(var key in rules)
    		{
    			for(var rule in rules[key])
    			{
    				if(error[key])
    					break;

    				if(rule == 'required')
            {
              result = validateRequired(key, store[key], null, rules[key][rule]);
              if(result !== true)
                error[key] = rules[key][rule];

              continue;
            }

    				if(/^length\([0-9]+\,[0-9]+\)$/.test(rule))
    				{
    					var params = /^length\(([0-9]+)\,([0-9]+)\)$/.exec(rule);
    					result = validateLength(key, store[key], {min:params[1], max:params[2]}, rules[key][rule]);
              if(result !== true)
                error[key] = rules[key][rule];

              continue;
    				}

    				if(/^length_min\([0-9]+\)$/.test(rule))
    				{
    					var params = /^length_min\(([0-9]+)\)$/.exec(rule);
    					result = validateLength(key, store[key], {min:params[1], max:null}, rules[key][rule]);
              if(result !== true)
                error[key] = rules[key][rule];

              continue;
    				}

    				if(/^length_max\([0-9]+\)$/.test(rule))
    				{
    					var params = /^length_max\(([0-9]+)\)$/.exec(rule);
    					result = validateLength(key, store[key], {min:null, max:params[1]}, rules[key][rule]);
              if(result !== true)
                error[key] = rules[key][rule];

              continue;
    				}

            if(/^greater\([0-9]+\)$/.test(rule))
            {
              var params = /^greater\(([0-9]+)\)$/.exec(rule);
              result = validateGreater(key, store[key], {than:params[1]}, rules[key][rule]);
              if(result !== true)
                error[key] = rules[key][rule];

              continue;
            }

            if(/^regex\(.+\)$/.test(rule))
            {
              var params = /^regex\((.+)\)$/.exec(rule);
              result = validateRegex(key, store[key], {pattern:new RegExp(params[1])}, rules[key][rule]);
              if(result !== true)
                error[key] = rules[key][rule];

              continue;
            }

            if(/^oneOf\(.+\)$/.test(rule))
            {
              var params = /^oneOf\((.+)\)$/.exec(rule);
              var values = _.map(params[1].split(','), function(v){return v.trim()});
              result = validateOneOf(key, store[key], {values:values}, rules[key][rule]);
              if(result !== true)
                error[key] = rules[key][rule];

              continue;
            }

    				if(rule == 'email')
            {
    					result = validateEmail(key, store[key], null, rules[key][rule]);
              if(result !== true)
                error[key] = rules[key][rule];

              continue;
            }

    				if(rule == 'integer')
            {
    					result = validateInteger(key, store[key], null, rules[key][rule]);
              if(result !== true)
                error[key] = rules[key][rule];

              continue;
            }

            if (rule == 'space') {
              result = validateSpace(key,store[key],null, rules[key][rule]);
              if (result !== true)
                error[key] = rules[key][value];
              continue;
            }

    				if(rule == 'file_required')
            {
    					result = validateFileRequired(key, store[key], null, rules[key][rule]);
              if(result !== true)
                error[key] = rules[key][rule];

              continue;
            }

    				if(/^file_type\(([a-z\/]+)\,?\)$/.test(rule))
    				{
    					var params = /^file_type\(([a-z]+)\,?\)$/.exec(rule);
    					params.splice(0,1);
    					result = validateLength(key, store[key], {types:params}, rules[key][rule]);
              if(result !== true)
                error[key] = rules[key][rule];

              continue;
    				}

    				if(/^file_size_max\([0-9]+\)$/.test(rule))
    				{
    					var params = /^file_size_max\(([0-9]+)\)$/.exec(rule);
    					result = validateLength(key, store[key], {min:null, max:params[1]}, rules[key][rule]);
              if(result !== true)
                error[key] = rules[key][rule];

              continue;
    				}

            deferred.reject(new Error('validation rule not supported'));
            return deferred.promise;
    			}
    		}

        if(Object.keys(error).length == 0)
          deferred.resolve(true);
        else
          deferred.resolve(error);

        return deferred.promise;

      }

    };

  };

  return {

    getInstance: function () {

      if ( !instance ) {
        instance = init();
      }

      return instance;
    }

  };

};

module.exports = exports = Validator;
var emailRegex = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
