var Auth = function(path, Q, crypto, uuid, _, store, utils, async){

    var instance;

    function init() {
        return {

            validateSignature: function(req, params){

                var deferred = Q.defer();

                var signature = req.get('W-Sign');
                var access_token = req.get('W-Access-Token');
                var timestamp = req.get('W-Timestamp');
                var nonce = req.get('W-Nonce');

                if(!signature || !access_token || !timestamp || !nonce){
                    deferred.reject({code:90, message:'FAIL_AUTH_HEADERS_MISSING'});
                    return deferred.promise;
                }

                var _app;
                var param_keys = [];
                store.findOne('applications', {access_token: access_token}).then(function(app){

                    if(!app){
                        throw {code:90, message:'FAIL_AUTH_APP_NOT_FOUND'};
                    }

                    _app = app;

                    return store.findOne('auth_nonce',{app_token: _app._id.toString(), nonce: nonce});

                }).then(function(auth){
                        if(auth)
                            throw {code:90, message:'FAIL_AUTH_NONCE'};

                        var auth_nonce = {
                            app_token: _app._id.toString(),
                            timestamp: parseInt(timestamp),
                            nonce: nonce,
                            ip_addr: req.headers['x-forwarded-for'] || req.connection.remoteAddress
                        };
                        return store.save('auth_nonce', auth_nonce);

                    }).then(function(auth){

                        for(var key in params)
                        {
                            param_keys.push(key);
                        }

                        param_keys.sort();

                        var sign_base = 'app_token=' + _app._id.toString();
                        sign_base += '&timestamp=' + timestamp;
                        sign_base += '&nonce=' + nonce;
                        sign_base += '&access_token=' + access_token;

                        for (var i = 0; i < param_keys.length; i++)
                        {
                            sign_base += '&' + param_keys[i] + '=' + params[param_keys[i]];
                        };
                        var check_signature = Utils.sign_request(sign_base, _app.client_secret);

                        if(check_signature != signature)
                            throw {code:90, message:'FAIL_AUTH_SIGN_CORRUPT'};

                        deferred.resolve(_app);

                    }).fail(function(error){
                        deferred.reject(error);
                        return;
                    });
                return deferred.promise;
            },

            validateSignatureWithParams: function(signature, access_token, timestamp, nonce, params){
                var deferred = Q.defer();
                if(!signature || !access_token || !timestamp || !nonce){
                    deferred.reject({code:90, message:'FAIL_AUTH_HEADERS_MISSING'});
                    return deferred.promise;
                }
                var _app;
                var param_keys = [];
                var params = params;
                store.findOne('applications',{access_token: access_token}).then(function(app){
                    if(!app){
                        throw {code:90, message:'FAIL_AUTH_APP_NOT_FOUND'};
                    }
                    _app = app;

                    return store.findOne('auth_nonce',{app_token: _app._id, nonce: nonce});

                }).then(function(auth){

                        if(auth){
                            throw {code:90, message:'FAIL_AUTH_NONCE'};
                        }

                        var auth_nonce = {
                            app_token: _app._id.toString(),
                            timestamp: parseInt(timestamp),
                            nonce: nonce
                        };
                        return store.save('auth_nonce', auth_nonce);

                    }).then(function(auth){
                        for(var key in params)
                        {
                            param_keys.push(key);
                        }
                        param_keys.sort();

                        var sign_base = 'app_token=' + _app._id;
                        sign_base += '&timestamp=' + timestamp;
                        sign_base += '&nonce=' + nonce;
                        sign_base += '&access_token=' + access_token;

                        for (var i = 0; i < param_keys.length; i++)
                        {
                            sign_base += '&' + param_keys[i] + '=' + params[param_keys[i]];
                        };
                        var check_signature = Utils.sign_request(sign_base, _app.client_secret);

                        if(check_signature != signature){
                            throw {code:90, message:'FAIL_AUTH_SIGN_CORRUPT'};
                        }

                        deferred.resolve(_app);

                    }).fail(function(error){

                        deferred.reject(error);
                        return;

                    });

                return deferred.promise;
            },


            addUserAgencyToObject: function(object, user){
                var deferred = Q.defer();

                if(object && user && user.agency){
                    object['agency'] = user.agency;
                    deferred.resolve(object);
                }
                else{
                    deferred.reject({code:99, message:'FAIL_AGENCY_NOT_EXIST'});
                }

                return deferred.promise;

            },

            addUserAgencyToQuery: function(query, user){
                var deferred = Q.defer();

                if(query && user && user.agency && user.agency === 'seamless'){
                    deferred.resolve(query);
                }

                else if(query && user && user.agency && user.agency !== 'seamless'){
                    query['agency'] = user.agency;
                    deferred.resolve(query);
                }
                else{
                    deferred.reject({code:99, message:'FAIL_AGENCY_NOT_EXIST'});
                }

                return deferred.promise;

            },

            validateUserForUpdate:function(user, apps) {

                var deferred = Q.defer();
                if (user.agency === 'seamless'){
                    deferred.resolve(true);
                    return deferred.promise;
                }
                else if (user) {

                    async.each(apps, function(app, callback){
                        store.findOne('applications', {'_id': app}).then(function(db_app){
                            if(db_app.agency !== user.agency){
                                deferred.resolve(false);
                                return deferred.promise;
                            }
                            callback();
                        });

                    }, function(error){
                        if(error) {
                            deferred.reject(false);
                            return deferred.promise;
                        }
                        deferred.resolve(true);
                    });

                }
                else{
                    deferred.resolve(false);
                }

                return deferred.promise;

            },

            userValidationForOperation: function (user, itemKey, type){

                if (user.role == 'admin') {
                    return true;
                } else {

                    if (user.appIdList.indexOf(itemKey) != -1) {
                        return true;
                    } else {
                        return false;
                    }
                }

            }

        };
    }

    return {

        getInstance: function () {

            if ( !instance ) {
                instance = init();
            }

            return instance;
        }

    };

};

module.exports = exports = Auth;
