var Utils = function (path, Q, crypto, mongoSkin) {

  var instance;

  function init() {

    return {

      toObjectID: function(val){
        return mongoSkin.helper.toObjectID(val);
      },

      base64Encode: function(txt) {
        return new Buffer(txt).toString('base64');
      },

      base64Decode: function(encoded) {
        return new Buffer(encoded, 'base64').toString('utf-8');
      },

      customBase64Encode: function(txt){

        var encoded = new Buffer(txt).toString('base64');
        //console.log(encoded);
        var random = Math.floor(Math.random() * 5 + 5);
        //console.log(random);
        var rx = new RegExp('.{1,' + random + '}', 'g');
        var chunks = encoded.match(rx);
        //console.log(chunks);
        var chunksUpdated = [];
        for (var i = 0; i < chunks.length; i++) {
          if(i > 0) {
            chunksUpdated.push(String.fromCharCode(Math.floor(Math.random() * 26) + 97));
          }
          chunksUpdated.push(chunks[i]);
        };
        //console.log(chunksUpdated);
        var chunksJoined = chunksUpdated.join('');
        chunksJoined = random.toString() + chunksJoined;
        //console.log(chunksJoined);
        return new Buffer(chunksJoined).toString('base64');

      },

      customBase64Decode: function(encoded){

        var decoded = new Buffer(encoded, 'base64').toString('utf-8');
        //console.log(decoded);
        var random = parseInt(decoded.charAt(0));
        //console.log(random);
        var chunksJoined = decoded.slice(1);
        //console.log(chunksJoined);
        var rx = new RegExp('.{1,' + (random + 1) + '}', 'g');
        var chunks = chunksJoined.match(rx);
        //console.log(chunks);
        var chunksUpdated = [];
        for (var i = 0; i < chunks.length; i++) {
          if(chunks[i].length <= random) {
            chunksUpdated.push(chunks[i]);
          } else {
            chunksUpdated.push(chunks[i].slice(0, -1));
          }
        };
        //console.log(chunksUpdated);
        var chunksJoined = chunksUpdated.join('');
        //console.log(chunksJoined);
        return new Buffer(chunksJoined, 'base64').toString('utf-8');

      },

      encrypt: function(algorithm, password, plain) {

        var keyBuf = new Buffer(Array(32));
        keyBuf.write(password, 'utf8');
        var ivBuf = new Buffer(Array(16));
        var cipher = crypto.createCipheriv(algorithm, keyBuf, ivBuf);
        //var cipher = crypto.createCipher(algorithm, password);
        var encrypted = cipher.update(plain, 'utf8', 'base64');
        encrypted += cipher.final('base64');
        return encrypted;

      },

      decrypt: function(algorithm, password, encrypted) {
        var keyBuf = new Buffer(Array(32));
        keyBuf.write(password, 'utf8');
        var ivBuf = new Buffer(Array(16));
        var decipher = crypto.createDecipheriv(algorithm, keyBuf, ivBuf);
        //var decipher = crypto.createDecipher(algorithm, password);
        var decrypted = decipher.update(encrypted, 'base64', 'utf8');
        decrypted += decipher.final('utf8');
        return decrypted;
      },

      exists_or_null: function (obj) {
        if (typeof obj == 'undefined' || !obj || obj.trim() == '')
          return null;
        return obj;
      },

      hash_password: function (str) {
        var md5sum = crypto.createHash('md5');
        md5sum.update(str);
        return md5sum.digest('hex');
      },

      generate_key: function () {
        var sha1sum = crypto.createHash('sha1');
        sha1sum.update(uuid.v4());
        return sha1sum.digest('hex');
      },

      save_file_upload: function (file_to_read, path_to_write) {
        var deferred = Q.defer();

        fs.readFile(file_to_read.path, function (error, data) {
          if (error) {
            deferred.reject(error);
            return;
          }

          fs.writeFile(path_to_write, data, function (error) {
            if (error) {
              deferred.reject(error);
              return;
            }

            deferred.resolve();
          });
        });

        return deferred.promise;
      },

      sign_request: function (token, key) {

        return crypto.createHmac('sha256', key).update(token).digest('hex');

      },

      readFile: function(filePath){

        var deferred = Q.defer();

        fs.readFile(filePath, {encoding:'utf-8'}, function (error, data) {

          if (error) {
            deferred.reject(error);
            return;
          }

          deferred.resolve(data);
          
        });

        return deferred.promise;


      }

    };
  }

  return {

    getInstance: function () {

      if (!instance) {
        instance = init();
      }

      return instance;
    }

  };

};


module.exports = exports = Utils;
